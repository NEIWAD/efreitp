package fr.idak.tutorial.ws.bigdatahadoop;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import java.io.IOException;


// Notre classe REDUCE - templatée avec un type générique K pour la clef, un type de valeur IntWritable, et un type de retour
// (le retour final de la fonction Reduce) Text.
public class AnagramReduce extends Reducer<Text, Text, Text, Text>
{
	private Text outputVal = new Text();

        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException
	{
			StringBuffer anagramsList = new StringBuffer();
			//print all anagrams on the same line
			for(Text val: values){
				anagramsList.append(val+" ");
			}
			outputVal.set(new String(anagramsList));
			context.write(key, outputVal);
        }
}