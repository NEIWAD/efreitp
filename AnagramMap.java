package fr.idak.tutorial.ws.bigdatahadoop;


import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.IntWritable;
import java.util.StringTokenizer;
import org.apache.hadoop.mapreduce.Mapper;
import java.io.IOException;
import java.util.Arrays;


// Notre classe MAP.
public class AnagramMap extends Mapper<Object, Text, Text, Text>
{
        private Text outputKey = new Text();
        private Text outputValue = new Text();

	// La fonction MAP elle-même.
        @Override
	protected void map(Object key, Text value, Context context) throws IOException, InterruptedException
	{  
            String line = value.toString();
            StringTokenizer tokenizer = new StringTokenizer(line);
            while(tokenizer.hasMoreTokens()){
                    String word = tokenizer.nextToken();
                    char[] wordChars = word.toCharArray();
                    Arrays.sort(wordChars);
                    //use sorted string as intermediate key
                    outputKey.set(new String(wordChars));
                    //use string itself as intermediate value
                    outputValue.set(word);
                    context.write(outputKey, outputValue);
            }
	}
}
